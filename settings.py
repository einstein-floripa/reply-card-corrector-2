import os

__DIR__ = os.path.dirname(os.path.realpath(__file__))

CSV_FOLDER = __DIR__ + '/csv_folder'

DEFAULT_OUTPUT_FOLDER = __DIR__ + '/default_output'

