import cv2 as cv


def distance_between(pt1, pt2):
    """ @param pt1 and pt2 : tupĺe of positions

        @return distance between two points
    """
    return ((pt1[0] - pt2[0]) ** 2 + (pt1[1] - pt2[1]) ** 2) ** (1 / 2)


def show_img(img):
    """ @param img: image to be displayed until key pressed

        @return: None
    """
    cv.imshow('Image', img)
    while True:
        if cv.waitKey(1) & 0xFF == ord('q'):
            break

    cv.destroyAllWindows()
