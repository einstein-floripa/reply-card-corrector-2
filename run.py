'''
    Main file, receive arguments and run reading,
    linking and correcting routines
'''

from os.path import isdir
from os import mkdir
from shutil import rmtree
import argparse

from loguru import logger

from reader.readers import (read_60Q, read_ENEM,
                            read_UDESC, read_UFSC)
import settings

test_options = {'UFSC': read_UFSC,
                'ENEM': read_ENEM,
                'UDESC': read_UDESC,
                '60Q': read_60Q}
default_output_folder = ''


if __name__ == '__main__':
    # Organize parameters to call specific routines
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('test',
                        help='Select which test you want to process. ' +
                             f'{test_options.keys()}',
                        type=str
                        )
    parser.add_argument('scans_folder',
                        help='Folder where .jpg files are placed',
                        type=str
                        )
    parser.add_argument('--output_folder',
                        help='Under output folder will be created all ' +
                             'output files and folder scheme, with temp ' +
                             'files. The folder must not exists, avoiding. ' +
                             'problems with deleting files acidentaly. If ' +
                             'use default folder, if it exists it will ' +
                             'be removed',
                        type=str
                        )
    args = parser.parse_args()

    if args.test not in test_options.keys():
        logger.error(f'Test option `{args.test}` not valid!')
        exit(1)

    if args.output_folder:
        if isdir(args.output_folder):
            logger.error(f'Folder `{args.output_folder}` already exists!')
            exit(1)

    else:
        args.output_folder = settings.DEFAULT_OUTPUT_FOLDER

        if isdir(args.output_folder):
            logger.info('Removing default output folder to place new one')
            rmtree(args.output_folder)

        mkdir(args.output_folder)

    if not isdir(args.scans_folder):
        logger.error('Input dir does not exist!')
        exit(1)

    logger.info(f'Calling routine for {args.test}')
