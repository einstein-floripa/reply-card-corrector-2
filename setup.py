from setuptools import setup

setup(
    name='Einsteinian reply card reader',
    version='2.0',
    packages=['reader', 'linker', ],
    license='MIT',
    author="Thiago Sant' Helena",
    author_email='thiago.sant.helena@gmail.com',
    description='A tool for making easier to read reply cards from tests stude'
                'nts. Uma automatização da leitura dos cartões resposta dos '
                'simulados executados com alunos do projeto Einstein Floripa',
    install_requires=[]
)
