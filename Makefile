build:
	@python setup.py build

sdist:
	@python setup.py sdist

install: sdist
	@python setup.py install

flake8:
	@flake8

mypy:
	@mypy . --ignore-missing-imports

bandit:
	@bandit . -r

check: flake8 mypy bandit


.PHONY: clean
clean:
	@rm -fR build dist *egg-info